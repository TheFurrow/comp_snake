'use strict';
var dbConnection = require('./appconf.js').dbConnection;

//Mysql connection
class Database {
  constructor() {
    var mysql = require('mysql');
        this.connection = mysql.createConnection({
          host:dbConnection.host,
          user:dbConnection.user,
          password:dbConnection.password,
          database:dbConnection.database
        });

      this.connection.connect();
  }
}

class Rankings extends Database {
  get(callback) {
    this.connection.query('SELECT * FROM rankings ORDER BY ranking DESC',function(err,rows) {
      if(err) {
        console.log('ERROR',err);
        throw err;
      }
      else {
        return callback(rows);
      }
    })
  };
}

class Chat extends Database {

    //Insert chat object data into chat table
    insert(object,callback) {
      //Try our code, we have set already one error throw
      try {
        //Query database and insert into chat table username, msg, date
        this.connection.query('INSERT INTO chat(userId,username,msg,msgDate) VALUES(?,?,?,NOW())', [object.userId,object.username, object.msg], function(err,result) {
          if(err) throw 'DB INSERT ERROR (35):'+err;
          else {
            console.log('New chat message saved');
            return callback(true);
          }
        })
      }
      //Catch error, return false callback with error message
      catch(err) {
        console.log(err)
        return callback(false,err);

      }
    }

}

module.exports.Chat = Chat;
module.exports.Rankings = Rankings;
