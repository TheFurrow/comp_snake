//Server variables
var express = require('express')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

//Classes
var Queries = require('./queries.js');

//Server functions
app.use('/', express.static(__dirname));

////////////////////////////////////////
///////////////WEBSOCKET////////////////
////////////////////////////////////////
io.on('connection',function(socket) {

  //Get rankings
  socket.on('/req_rankings', function() {
    var rank = new Queries.Rankings();
    rank.get(function(callback) {
      socket.emit('/res_rankings',callback);
    })
  })

  //Listens on requests to write in chat
  socket.on('/req_writeChat',function(msgObject) {
    var chat = new Queries.Chat();
    //We will pass JSON object to our insert() function inside of
    //Chat class
    chat.insert(msgObject,function(status,errorMsg) {
      //insert() function returns us callback with status value (boolean)
      //status false means that there was problem with insert
      //status true means that chat message was written successfully

      //send error only to client if false
      if(status == false) socket.emit('/res_chatMsg', errorMsg);
      //broadcast if true
      else socket.broadcast.emit('/res_chatMsg', msgObject);
    });
  });
});

////////////////////////////////////////
///////////// GET //////////////////////
////////////////////////////////////////



////////////////////////////////////////
///////////// RUN SERVER ///////////////
////////////////////////////////////////
http.listen('3000', function() {
  console.log('Snake is listening localhost:3000');
})
