//Load socket.io
  var socket = io();
//This is executed when document is ready
$(document).ready(function() {

  //Receive chat messages
  socket.on('/res_chatMsg',function(msgObject) {
    console.log(msgObject);
    //Do something with message

    //Put message on chatbox -- EXAMPLE
    $('#messagebox').append('<span><b>'+msgObject.username+'</b>&nbsp;'+msgObject.msg+'</span><br/>');
  })

  //Listen key presses and launch function
  $('#chatmessage').keypress(function(e) {
    //Act if keycode = Enter
    if(e.which == 13) {
      console.log()
      //Construct message object
      var object = {
        userId: 1,
        username: 'paavali',
        msg: $(this).val()
      }

      //Put message on chatbox
      $('#messagebox').append('<span><b>'+object.username+'</b>&nbsp;'+object.msg+'</span><br/>');
      //Empty input
      $(this).val('');

      //Emit chat object
      socket.emit('/req_writeChat', object);
    }
  })


  /*socket.on('/req_writeChat', function(msg) {
    console.log(msg);
  })*/
})
